package ru.goryachev.testjobkassa.presenters;


import android.content.Intent;

import java.sql.SQLException;
import java.util.List;

import ru.goryachev.testjobkassa.helpers.HelperFactory;
import ru.goryachev.testjobkassa.models.CheckHead;
import ru.goryachev.testjobkassa.models.CheckPos;
import ru.goryachev.testjobkassa.ui.activities.CheckActivity;
import ru.goryachev.testjobkassa.ui.activities.MainActivity;

public class MainActivityPresenter implements IMainActivityPresenter {

    private MainActivity mainActivity;

    public MainActivityPresenter(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    @Override
    public void newReport() {

    }

    @Override
    public void newCheck() {
        mainActivity.startActivity(new Intent(mainActivity, CheckActivity.class));
    }

    @Override
    public List<CheckHead> getAllChecks() throws SQLException {
        return HelperFactory.getHelper().getCheckHeadDAO().getAllChecks();

    }

    @Override
    public List<CheckPos> getAllCheckPosByCheckHead(Integer checkHeadId) throws SQLException {
        return HelperFactory.getHelper().getCheckPosDAO().findAllCheckPosByHeadId(checkHeadId);
    }
}
