package ru.goryachev.testjobkassa.presenters;

import android.util.Log;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ru.goryachev.testjobkassa.helpers.HelperFactory;
import ru.goryachev.testjobkassa.models.CheckHead;
import ru.goryachev.testjobkassa.models.CheckPos;
import ru.goryachev.testjobkassa.models.Good;
import ru.goryachev.testjobkassa.ui.activities.CheckActivity;

public class CheckPresenter implements ICheckPresenter {
    private CheckActivity checkActivity;
    private CheckHead checkHead;
    private List<CheckPos> checkPosList;

    private static final String TAG = "CheckPresenter";

    public CheckPresenter(CheckActivity checkActivity) {
        this.checkActivity = checkActivity;
        checkHead = new CheckHead();
        Log.d(TAG, "check id " + checkHead.id);
        Log.d(TAG, "check number " + checkHead.number);
        checkPosList = new ArrayList<>();
    }

    // Добавляет товар в "корзину", при повторном добавлении увеличивает количество
    @Override
    public void addGood(Good good, double price) {
        for (CheckPos tempCheckPos : checkPosList) {
            if (tempCheckPos.good.idGood == good.idGood) {
                tempCheckPos.quant += 1;
                tempCheckPos.summPos += price;
                checkActivity.updateGoodsList();
                return;
            }
        }
        CheckPos checkPos = new CheckPos();
        checkPos.good = good;
        checkPos.summPos += price;
        checkPos.id_check = checkHead;
        checkPos.summPos = good.price * checkPos.quant;
        checkPosList.add(checkPos);

        checkActivity.updateGoodsList();
    }

    @Override
    public synchronized void removeCheckPos(CheckPos checkPos) {
        checkPosList.remove(checkPos);
        checkActivity.updateGoodsList();
    }

    @Override
    public void saveCheck() throws SQLException {
//        Сохраняем только в том случае, если в чеке есть позиции
        if (checkPosList.size() > 0) {
            checkHead.datetime = new Timestamp(new Date().getTime());
            checkHead.summ += getCheckSumm();
            HelperFactory.getHelper().getCheckHeadDAO().create(checkHead);
            checkHead.number = checkHead.id;
            HelperFactory.getHelper().getCheckHeadDAO().update(checkHead);
//        Сохраняем в БД позиции чека
            for (CheckPos savingCheckPos : checkPosList) {
                savingCheckPos.id_check = checkHead;
                HelperFactory.getHelper().getCheckPosDAO().create(savingCheckPos);
            }
        }

    }

    //    Сумма всех товаров в "корзине"
    private double getCheckSumm() {
        double summ = 0;
        for (CheckPos tempCheckPos : checkPosList) {
            summ += tempCheckPos.quant * tempCheckPos.good.price;
        }
        return summ;
    }



    @Override
    public void setDirectionToModel(Integer direction) {
        checkHead.direction = direction;
    }

    public List<CheckPos> getCheckPosList() {
        return checkPosList;
    }
}
