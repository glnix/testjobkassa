package ru.goryachev.testjobkassa.presenters;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

public interface IReportActivityPresenter {

    List<String[]> getReportEntries(Date startDate, Date endDate) throws SQLException;
}
