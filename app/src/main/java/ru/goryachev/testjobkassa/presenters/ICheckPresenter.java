package ru.goryachev.testjobkassa.presenters;

import java.sql.SQLException;

import ru.goryachev.testjobkassa.models.CheckPos;
import ru.goryachev.testjobkassa.models.Good;

public interface ICheckPresenter {

    void addGood(Good good, double price);

    void removeCheckPos(CheckPos checkPos);

    void saveCheck() throws SQLException;

    void setDirectionToModel(Integer direction);

}
