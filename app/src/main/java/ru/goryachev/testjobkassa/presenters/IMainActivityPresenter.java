package ru.goryachev.testjobkassa.presenters;

import java.sql.SQLException;
import java.util.List;

import ru.goryachev.testjobkassa.models.CheckHead;
import ru.goryachev.testjobkassa.models.CheckPos;

public interface IMainActivityPresenter {
    void newReport();

    void newCheck();

    List<CheckHead> getAllChecks() throws SQLException;

    List<CheckPos> getAllCheckPosByCheckHead(Integer checkHeadId) throws SQLException;

}
