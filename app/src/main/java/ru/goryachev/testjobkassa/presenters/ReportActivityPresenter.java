package ru.goryachev.testjobkassa.presenters;


import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import ru.goryachev.testjobkassa.helpers.HelperFactory;

public class ReportActivityPresenter implements IReportActivityPresenter {

    @Override
    public List<String[]> getReportEntries(Date startDate, Date endDate) throws SQLException {
        List<String[]> results = HelperFactory.getHelper().getCheckHeadDAO().getReportForDateInterval(startDate, endDate);
        return results;
    }
}
