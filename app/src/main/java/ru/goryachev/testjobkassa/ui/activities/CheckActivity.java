package ru.goryachev.testjobkassa.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;

import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.goryachev.testjobkassa.R;
import ru.goryachev.testjobkassa.adapters.CheckPosListAdater;
import ru.goryachev.testjobkassa.adapters.LinkedHashMapAdapter;
import ru.goryachev.testjobkassa.helpers.HelperFactory;
import ru.goryachev.testjobkassa.models.Barcode;
import ru.goryachev.testjobkassa.models.CheckTypes;
import ru.goryachev.testjobkassa.presenters.CheckPresenter;
import ru.goryachev.testjobkassa.ui.dialogs.YesNoDialog;

public class CheckActivity extends AppCompatActivity implements ICheckActivity, YesNoDialog.NoticeDialogListener {

    private static final String TAG = "CheckActivity";
    private static final int REQUEST_CODE_BARCODE = 1;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.fab)
    FloatingActionButton fab;
    @Bind(R.id.coordinator_layout)
    CoordinatorLayout coordinatorLayout;
    @Bind(R.id.check_type_spinner)
    AppCompatSpinner typeSpinner;
    @Bind(R.id.add_button)
    AppCompatButton addButton;
    @Bind(R.id.goods_list)
    RecyclerView checkPosList;

    CheckPresenter presenter;
    CheckPosListAdater checkPosListAdater;

    LinkedHashMap<Integer, String> checkDirectionsMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        presenter = new CheckPresenter(this);
        setSpinner();
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
//        TODO либо презентер, либо данные
        checkPosListAdater = new CheckPosListAdater(this, presenter.getCheckPosList(), presenter);
        checkPosList.setLayoutManager(layoutManager);
        checkPosList.setAdapter(checkPosListAdater);
    }

    //  Заполним выпадающий список типами чека
    private void setSpinner() {

        checkDirectionsMap = CheckTypes.getDirectionsTypes();

        final LinkedHashMapAdapter adapter = new LinkedHashMapAdapter<>(getApplicationContext(), R.layout.spinner_item_check_type, checkDirectionsMap);
        adapter.setDropDownViewResource(R.layout.spinner_item_check_type);

        typeSpinner.setAdapter(adapter);
        typeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Map.Entry item = adapter.getItem(position);
                presenter.setDirectionToModel((Integer) item.getKey());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    //    Закрыть чек
    @OnClick(R.id.fab)
    public void onFabClick() {
        try {
            presenter.saveCheck();
            finish();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void showMessage(String msg) {
        Snackbar.make(coordinatorLayout, msg, Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
    }

    @Override
    public void updateGoodsList() {
        checkPosListAdater.notifyDataSetChanged();
    }

    @Override
    public int getDirection() {
        return 0;
    }


    //    При нажатии кнопки назад спросим у пользователя уверен ли он в отмене чека
    @Override
    public void onBackPressed() {
        YesNoDialog dialog = YesNoDialog.newInstance(getString(R.string.text_are_you_sure), getString(R.string.text_check_create_cancel));
        dialog.show(getSupportFragmentManager(), null);
    }


    //    Два итерфейса для взаимодействия с диалогом YesNoDialog
    @Override
    public void onDialogPositiveClick(DialogFragment dialogFragment) {
        Log.d(TAG, "onDialogPositiveClick");
        finish();
    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialogFragment) {
        Log.d(TAG, "onDialogNegativeClick");
        dialogFragment.dismiss();
    }

    @OnClick(R.id.add_button)
    public void onAddButtonClick() {
        Intent i = new Intent(this, AddGoodsActivity.class);
        startActivityForResult(i, REQUEST_CODE_BARCODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            Log.d(TAG, "Result OK");
            //        Если пришел штрихкод
            if (requestCode == REQUEST_CODE_BARCODE) {
                String barcode = data.getStringExtra("Barcode");
                try {
                    Barcode barcodeObj = HelperFactory.getHelper().getBarcodesDAO().findBarcodeByCode(barcode);
                    if (barcodeObj != null) {
                        Log.d(TAG, "Price: " + barcodeObj.price);
                        presenter.addGood(barcodeObj.getGood(), barcodeObj.price);
//                        Если товара с таким кодом нет в БД сообщаем об этом пользователю
                    } else showMessage(getString(R.string.error_text_goods_not_found));
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                YesNoDialog dialog = YesNoDialog.newInstance(getString(R.string.text_are_you_sure), getString(R.string.text_check_create_cancel));
                dialog.show(getSupportFragmentManager(), null);
                break;
        }
        return true;
    }
}
