package ru.goryachev.testjobkassa.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.google.zxing.Result;

import butterknife.Bind;
import butterknife.ButterKnife;
import me.dm7.barcodescanner.zxing.ZXingScannerView;
import ru.goryachev.testjobkassa.R;

public class AddGoodsActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {

    @Bind(R.id.scanner_view)
    ZXingScannerView scannerView;
    @Bind(R.id.barcode_input)
    AppCompatEditText barcodeInput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_goods);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void handleResult(Result result) {
//        Если штрих код считан камерой, поставим его в поле ввода
        if (result != null && result.getText().length() > 0) {
            barcodeInput.setText(result.getText());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        scannerView.setResultHandler(this);
        scannerView.startCamera();
    }

    @Override
    protected void onPause() {
        super.onPause();
        scannerView.stopCamera();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_add_goods, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
//        Шрихкод введен
        Log.d(this.getClass().getSimpleName(), "onOptionsItemSelected " + item.getTitle());
        if (id == R.id.action_done) {
            Log.d(this.getClass().getSimpleName(), "R.id.action_done");
            Intent intent = new Intent();
            String barcodeString = barcodeInput.getText().toString().trim();
            if (barcodeString.length() > 0) {
                Log.d(this.getClass().getSimpleName(), "barcodeString.length() > 0");
                intent.putExtra("Barcode", barcodeString);
                setResult(RESULT_OK, intent);
            }
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
