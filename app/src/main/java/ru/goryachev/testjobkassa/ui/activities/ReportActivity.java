package ru.goryachev.testjobkassa.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import ru.goryachev.testjobkassa.R;
import ru.goryachev.testjobkassa.adapters.ReportListAdapter;
import ru.goryachev.testjobkassa.presenters.IReportActivityPresenter;
import ru.goryachev.testjobkassa.presenters.ReportActivityPresenter;

public class ReportActivity extends AppCompatActivity {

    IReportActivityPresenter presenter;

    @Bind(R.id.report_results_list)
    RecyclerView reportList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        presenter = new ReportActivityPresenter();
        showReportResults();
    }

    public void showReportResults() {
        Intent i = getIntent();
        Date startDate = new Date(i.getLongExtra("StartDate", 0));
        Date endDate = new Date(i.getLongExtra("EndDate", 0));
        try {
            List<String[]> results = presenter.getReportEntries(startDate, endDate);
            LinearLayoutManager layoutManager = new LinearLayoutManager(this);
            ReportListAdapter reportListAdapter = new ReportListAdapter(this, results);
            reportList.setLayoutManager(layoutManager);
            reportList.setAdapter(reportListAdapter);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
