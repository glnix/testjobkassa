package ru.goryachev.testjobkassa.ui.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

public class InfoDialog extends DialogFragment {

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(getArguments().getString("Message", ""))
                .setTitle(getArguments().getString("Title", ""))
                .setCancelable(true)
                .setPositiveButton("ОК", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        return builder.create();
    }

    public static InfoDialog newInstance(@Nullable String title, @NonNull String msg) {
        InfoDialog dialog = new InfoDialog();

        Bundle args = new Bundle();
        args.putString("Title", title);
        args.putString("Message", msg);
        dialog.setArguments(args);

        return dialog;
    }
}
