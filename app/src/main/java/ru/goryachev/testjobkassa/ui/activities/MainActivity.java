package ru.goryachev.testjobkassa.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.borax12.materialdaterangepicker.date.DatePickerDialog;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.goryachev.testjobkassa.R;
import ru.goryachev.testjobkassa.adapters.CheckListAdapter;
import ru.goryachev.testjobkassa.helpers.HelperFactory;
import ru.goryachev.testjobkassa.models.CheckHead;
import ru.goryachev.testjobkassa.presenters.MainActivityPresenter;

public class MainActivity extends AppCompatActivity implements IMainActivity, DatePickerDialog.OnDateSetListener {

    private final static String TAG = "MainActivity_LOG";

    @Bind(R.id.fab)
    FloatingActionButton fab;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.recycler_view_check_list)
    RecyclerView checkList;
    @Bind(R.id.coordinator_layout)
    CoordinatorLayout coordinatorLayout;

    MainActivityPresenter presenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        presenter = new MainActivityPresenter(this);
        setSupportActionBar(toolbar);

    }

    @Override
    protected void onResume() {
        super.onResume();
        fillCheckList();

//        Test
        try {
            HelperFactory.getHelper().getCheckHeadDAO().getReportForDateInterval(new Date(), new Date());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    private void fillCheckList() {
        try {
            List<CheckHead> checkHeads = presenter.getAllChecks();
            LinearLayoutManager layoutManager = new LinearLayoutManager(this);
            CheckListAdapter checkListAdapter = new CheckListAdapter(this, checkHeads, presenter);
            checkList.setLayoutManager(layoutManager);
            checkList.setAdapter(checkListAdapter);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_report) {
//            Запросим даты диалога
            showDateRangePickerDialog();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void showDateRangePickerDialog() {
//      По умолчанию дата начала и конца отчета - текущая дата
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                MainActivity.this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
//        Максимальная дата отчета - сегодня
        dpd.setMaxDate(Calendar.getInstance());
//        Локализуем диалог
        dpd.setStartTitle(getString(R.string.date_range_picker_dialog_start));
        dpd.setEndTitle(getString(R.string.date_range_picker_dialog_end));

        dpd.show(getFragmentManager(), "Datepickerdialog");
    }

    //    Действие для кнопки нового чека
    @OnClick(R.id.fab)
    public void onFabClick() {
        presenter.newCheck();
    }


    //    Событие от диалога выбора интервала дат для отчета
    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth, int yearEnd, int monthOfYearEnd, int dayOfMonthEnd) {
        Date startReportDate = new GregorianCalendar(year, monthOfYear, dayOfMonth).getTime();
        Date endReportDate = new GregorianCalendar(yearEnd, monthOfYearEnd, dayOfMonthEnd).getTime();

//        Если дата конца отчета ранее даты начала, сообщим об этом пользователю
        if (startReportDate.after(endReportDate)) {
            showMessage("Дата конца отчета ранее даты начала");
        } else {
            Intent i = new Intent(this, ReportActivity.class);
            i.putExtra("StartDate", startReportDate.getTime());
            i.putExtra("EndDate", endReportDate.getTime());
            startActivity(i);
        }

    }

    @Override
    public void showMessage(String msg) {
        Snackbar.make(coordinatorLayout, msg, Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
    }

}
