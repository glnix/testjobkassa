package ru.goryachev.testjobkassa.ui.activities;

public interface ICheckActivity {
    void showMessage(String msg);

    void updateGoodsList();

    int getDirection();
}
