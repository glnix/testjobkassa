package ru.goryachev.testjobkassa;

import android.app.Application;

import ru.goryachev.testjobkassa.helpers.HelperFactory;

public class TestJobKassaApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        HelperFactory.setHelper(getApplicationContext());
    }

    @Override
    public void onTerminate() {
        HelperFactory.releaseHelper();
        super.onTerminate();
    }
}
