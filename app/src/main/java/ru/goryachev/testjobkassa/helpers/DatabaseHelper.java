package ru.goryachev.testjobkassa.helpers;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.SQLException;

import ru.goryachev.testjobkassa.dao.BarcodesDAO;
import ru.goryachev.testjobkassa.dao.CheckHeadDAO;
import ru.goryachev.testjobkassa.dao.CheckPosDAO;
import ru.goryachev.testjobkassa.dao.GoodsDAO;
import ru.goryachev.testjobkassa.models.Barcode;
import ru.goryachev.testjobkassa.models.CheckHead;
import ru.goryachev.testjobkassa.models.CheckPos;
import ru.goryachev.testjobkassa.models.Good;

public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    private static final String TAG = DatabaseHelper.class.getSimpleName();
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "testdb.db";
    private static final String DATABASE_PATH = "/data/data/ru.goryachev.testjobkassa/databases/";
    private BarcodesDAO barcodesDAO;
    private GoodsDAO goodsDAO;
    private CheckHeadDAO checkHeadDAO;
    private CheckPosDAO checkPosDAO;


    public DatabaseHelper(Context context) {
        super(context, DATABASE_PATH + DATABASE_NAME, null, DATABASE_VERSION);

        boolean dbexist = checkdatabase();
        if (!dbexist) {
            // Если БД не существует, копируем ее из assets
            try {
                File dir = new File(DATABASE_PATH);
                dir.mkdirs();
                InputStream myinput = context.getAssets().open(DATABASE_NAME);
                String outfilename = DATABASE_PATH + DATABASE_NAME;
                OutputStream myoutput = new FileOutputStream(outfilename);
                byte[] buffer = new byte[1024];
                int length;
                while ((length = myinput.read(buffer)) > 0) {
                    myoutput.write(buffer, 0, length);
                }
                myoutput.flush();
                myoutput.close();
                myinput.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVer,
                          int newVer) {

    }

    //      Cинглтоны для DAO, как того требует ORMLite
    public synchronized BarcodesDAO getBarcodesDAO() throws SQLException {
        if (barcodesDAO == null) {
            barcodesDAO = new BarcodesDAO(getConnectionSource(), Barcode.class);
        }
        return barcodesDAO;
    }

    public synchronized GoodsDAO getGoodsDAO() throws SQLException {
        if (goodsDAO == null) {
            goodsDAO = new GoodsDAO(getConnectionSource(), Good.class);
        }
        return goodsDAO;
    }


    public synchronized CheckHeadDAO getCheckHeadDAO() throws SQLException {
        if (checkHeadDAO == null) {
            checkHeadDAO = new CheckHeadDAO(getConnectionSource(), CheckHead.class);
        }
        return checkHeadDAO;
    }

    public synchronized CheckPosDAO getCheckPosDAO() throws SQLException {
        if (checkPosDAO == null) {
            checkPosDAO = new CheckPosDAO(getConnectionSource(), CheckPos.class);
        }
        return checkPosDAO;
    }

    @Override
    public void close() {
        super.close();

    }

    //      Проверяет существует ли БД
    private boolean checkdatabase() {
        boolean checkdb = false;

        String myPath = DATABASE_PATH + DATABASE_NAME;
        File dbfile = new File(myPath);
        checkdb = dbfile.exists();

//        Log.i(DatabaseHelper.class.getName(), "DB Exist : " + checkdb);

        return checkdb;
    }
}
