package ru.goryachev.testjobkassa.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import ru.goryachev.testjobkassa.R;
import ru.goryachev.testjobkassa.models.CheckHead;
import ru.goryachev.testjobkassa.models.CheckPos;
import ru.goryachev.testjobkassa.models.CheckTypes;
import ru.goryachev.testjobkassa.presenters.MainActivityPresenter;
import ru.goryachev.testjobkassa.utils.AndroidUtils;

public class CheckListAdapter extends RecyclerView.Adapter<CheckListAdapter.ViewHolder> {
    private static final String TAG = "CheckListAdapter";
    private Context ctx;
    List<CheckHead> checkList;
    MainActivityPresenter presenter;

    public CheckListAdapter(Context ctx, List<CheckHead> checkList, MainActivityPresenter presenter) {
        this.ctx = ctx;
        this.checkList = checkList;
        this.presenter = presenter;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(ctx).inflate(R.layout.item_check_list, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        CheckHead checkHead = checkList.get(position);
        holder.number.setText("№ " + String.valueOf(checkHead.number));

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");
        Date checkDate = checkHead.datetime;
        Log.d(TAG, "Check time :"+checkDate);
        holder.date.setText(simpleDateFormat.format(checkDate));
        try {
            List<CheckPos> checkPoses = presenter.getAllCheckPosByCheckHead(checkHead.id);
            holder.positionsNumber.setText(ctx.getString(R.string.text_number_of_positions_in_check) + String.valueOf(checkPoses.size()));
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            String strSum = new DecimalFormat("#.00").format(checkHead.summ);
//        Минус указывает пользователю на то, что это возврат, в БД это отдельный признак
            if (checkHead.direction == CheckTypes.REFUND) strSum = new String("-").concat(strSum);
            holder.sum.setText(strSum + " " + ctx.getString(R.string.rubleSymbolJava));
            AndroidUtils.setRubleFont(holder.sum, ctx);
        }
    }

    @Override
    public int getItemCount() {
        return checkList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView number, date, sum, positionsNumber;

        public ViewHolder(View itemView) {
            super(itemView);
            number = (TextView) itemView.findViewById(R.id.check_number);
            date = (TextView) itemView.findViewById(R.id.date);
            positionsNumber = (TextView) itemView.findViewById(R.id.positions_number);
            sum = (TextView) itemView.findViewById(R.id.check_sum);
        }
    }
}
