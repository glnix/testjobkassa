package ru.goryachev.testjobkassa.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import ru.goryachev.testjobkassa.R;

public class ReportListAdapter extends RecyclerView.Adapter<ReportListAdapter.ViewHolder> {
    private static final String TAG = "ReportListAdapter";
    private Context ctx;
    List<String[]> results;

    public ReportListAdapter(Context ctx, List<String[]> results) {
        this.ctx = ctx;
        this.results = results;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(ctx).inflate(R.layout.item_report, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        String[] resultsRow = results.get(position);
        holder.date.setText(resultsRow[0]);
        holder.refunds.setText(resultsRow[1]);
        holder.sales.setText(resultsRow[2]);
        holder.total.setText(resultsRow[3]);
    }

    @Override
    public int getItemCount() {
        return results.size();
    }

    //  реализация паттерна ViewHolder
    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView date, refunds, sales, total;

        public ViewHolder(View itemView) {
            super(itemView);
            date = (TextView) itemView.findViewById(R.id.date);
            refunds = (TextView) itemView.findViewById(R.id.refunds);
            sales = (TextView) itemView.findViewById(R.id.sales);
            total = (TextView) itemView.findViewById(R.id.total);
        }
    }
}
