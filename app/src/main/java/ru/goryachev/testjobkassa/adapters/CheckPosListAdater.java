package ru.goryachev.testjobkassa.adapters;

import android.content.Context;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.List;

import ru.goryachev.testjobkassa.R;
import ru.goryachev.testjobkassa.models.CheckPos;
import ru.goryachev.testjobkassa.presenters.ICheckPresenter;
import ru.goryachev.testjobkassa.utils.AndroidUtils;

public class CheckPosListAdater extends RecyclerView.Adapter<CheckPosListAdater.ViewHolder> {
    private static final String TAG = "CheckListAdapter";
    private Context ctx;
    List<CheckPos> checkPosList;
    ICheckPresenter presenter;

    public CheckPosListAdater(Context ctx, List<CheckPos> checkPosList, ICheckPresenter presenter) {
        this.ctx = ctx;
        this.checkPosList = checkPosList;
        this.presenter = presenter;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(ctx).inflate(R.layout.item_goods_list, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final CheckPos checkPos = checkPosList.get(position);
        holder.name.setText(checkPos.good.name);
        double dQuant = checkPosList.get(position).quant;
        String strQuant = String.valueOf(new DecimalFormat("#").format(dQuant));
        holder.quant.setText(strQuant);
        String strPrice = String.valueOf(checkPos.good.price).concat(ctx.getString(R.string.rubleSymbolJava));
        holder.price.setText(strPrice);
        AndroidUtils.setRubleFont(holder.price, ctx);


//        Действия конопок изменения количества
        holder.plusBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkPosList.get(position).quant += 1;
                notifyDataSetChanged();
            }
        });
        holder.minusBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CheckPos checkPos = checkPosList.get(position);
                if (checkPos.quant == 1) {
                    presenter.removeCheckPos(checkPos);
                } else {
                    checkPos.quant -= 1;
                    notifyDataSetChanged();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return checkPosList.size();
    }

    //  реализация паттерна ViewHolder
    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView name, quant, price;
        public AppCompatButton plusBtn, minusBtn;

        public ViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name);
            price = (TextView) itemView.findViewById(R.id.price);
            quant = (TextView) itemView.findViewById(R.id.quant);
            plusBtn = (AppCompatButton) itemView.findViewById(R.id.plus_button);
            minusBtn = (AppCompatButton) itemView.findViewById(R.id.minus_button);
        }
    }
}
