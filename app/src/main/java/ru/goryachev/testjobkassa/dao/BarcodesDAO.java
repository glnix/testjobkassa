package ru.goryachev.testjobkassa.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;

import ru.goryachev.testjobkassa.models.Barcode;

public class BarcodesDAO extends BaseDaoImpl<Barcode, Integer> {
    public BarcodesDAO(ConnectionSource connectionSource, Class<Barcode> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    public Barcode findBarcodeByCode(String barcode) throws SQLException {
        return this.queryBuilder().where().eq("bar_code", barcode).queryForFirst();
    }
}
