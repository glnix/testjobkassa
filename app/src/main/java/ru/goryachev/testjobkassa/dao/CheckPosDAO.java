package ru.goryachev.testjobkassa.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.List;

import ru.goryachev.testjobkassa.models.CheckPos;

public class CheckPosDAO extends BaseDaoImpl<CheckPos, Integer> {
    public CheckPosDAO(ConnectionSource connectionSource, Class<CheckPos> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    public List<CheckPos> findAllCheckPosByHeadId(Integer checkHeadId) throws SQLException {
        return this.queryBuilder().where().eq("id_check", checkHeadId).query();
    }
}
