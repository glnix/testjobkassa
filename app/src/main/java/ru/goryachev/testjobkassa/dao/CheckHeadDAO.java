package ru.goryachev.testjobkassa.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.DatabaseTableConfig;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import ru.goryachev.testjobkassa.models.CheckHead;

public class CheckHeadDAO extends BaseDaoImpl<CheckHead, Long> {
    public CheckHeadDAO(ConnectionSource connectionSource, Class<CheckHead> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    //  Получить все созданные чеки
    public List<CheckHead> getAllChecks() throws SQLException {
        return this.queryForAll();
    }

    //    Формирует данные для отчета
    public List<String[]> getReportForDateInterval(Date dateStart, Date dateEnd) throws SQLException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String queryStartDate = "\"".concat(simpleDateFormat.format(dateStart)).concat("\"");
        String queryStartEnd = "\"".concat(simpleDateFormat.format(dateEnd)).concat("\"");
        String tableName = DatabaseTableConfig.extractTableName(this.dataClass);

        GenericRawResults<String[]> rawResults = this.queryRaw("select date(datetime),"
                + " sum(case when direction < 0 then summ else 0 end),"
                + " sum(case when direction > 0 then summ else 0 end),"
                + " sum(summ*direction)"
                + " from "
                + tableName
                + " where strftime('%Y-%m-%d', datetime) between " + queryStartDate
                + " AND " + queryStartEnd
                + " group by date(datetime) order by date(datetime)");
        List<String[]> results = rawResults.getResults();

        return results;
    }
}
