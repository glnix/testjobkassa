package ru.goryachev.testjobkassa.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;

import ru.goryachev.testjobkassa.models.Good;

public class GoodsDAO extends BaseDaoImpl<Good, Long> {
    public GoodsDAO(ConnectionSource connectionSource, Class<Good> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

}
