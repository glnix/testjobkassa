package ru.goryachev.testjobkassa.models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "goods")
public class Good {
    @DatabaseField(columnName = "name_good")
    public String name;
    @DatabaseField(columnName = "id_good", id = true)
    public int idGood;
    @DatabaseField(columnName = "price")
    public double price;


    public Good() {
    }
}
