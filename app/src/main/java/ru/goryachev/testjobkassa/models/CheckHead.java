package ru.goryachev.testjobkassa.models;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

@DatabaseTable(tableName = "check_head")
public class CheckHead {
    @DatabaseField(columnName = "id_check", generatedId = true)
    public int id;
    @DatabaseField(columnName = "number")
    public int number;
    @DatabaseField(columnName = "datetime", dataType = DataType.DATE)
    public Date datetime;
    @DatabaseField(columnName = "summ")
    public double summ;
    @DatabaseField(columnName = "direction")
    public int direction;

    public CheckHead() {
    }

    @Override
    public String toString() {
        return "CheckHead{" +
                "id=" + id +
                ", number=" + number +
                ", datetime=" + datetime +
                ", summ=" + summ +
                ", direction=" + direction +
                '}';
    }
}
