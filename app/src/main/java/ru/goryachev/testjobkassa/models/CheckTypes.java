package ru.goryachev.testjobkassa.models;


import java.util.LinkedHashMap;

// Типы чеков
public class CheckTypes {
    private CheckTypes() {
    }

    //    Продажа
    public static final Integer ORDER = 1;
    //    Возврат
    public static final Integer REFUND = -1;

    //  Названия для кодов типов чека
    public static LinkedHashMap<Integer, String> getDirectionsTypes() {
        LinkedHashMap<Integer, String> checkDirectionsMap = new LinkedHashMap<>();

        checkDirectionsMap.put(ORDER, "Новый чек");
        checkDirectionsMap.put(REFUND, "Возврат");
        return checkDirectionsMap;
    }
}
