package ru.goryachev.testjobkassa.models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "check_pos")
public class CheckPos {
    @DatabaseField(columnName = "num_pos", generatedId = true)
    public int numPos;
    @DatabaseField(columnName = "id_check", foreign = true, foreignAutoRefresh = true, unique = false)
    public CheckHead id_check;
    @DatabaseField(columnName = "quant")
    public double quant;
    @DatabaseField(columnName = "summ_pos")
    public double summPos;
    @DatabaseField(columnName = "id_good", foreign = true, foreignAutoRefresh = true)
    public Good good;

    public CheckPos() {
        quant = 1;
    }
}
