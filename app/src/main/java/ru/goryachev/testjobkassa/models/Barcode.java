package ru.goryachev.testjobkassa.models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "barcodes")
public class Barcode {
    @DatabaseField(columnName = "id_bar_code", id = true)
    public Integer id;
    @DatabaseField(columnName = "bar_code")
    public String barcode;
    @DatabaseField(columnName = "id_good", foreign = true, foreignAutoRefresh = true)
    public Good idGood;
    @DatabaseField(columnName = "bar_code_price")
    public double price;

    public Barcode(int id, String barcode, double price, double quantity, Good idGood, int unused, int askQuatity, int askSerial) {
        this.id = id;
        this.barcode = barcode;
        this.price = price;
        this.idGood = idGood;
    }

    public Barcode() {
    }

    public Good getGood() {
        return idGood;
    }
}
